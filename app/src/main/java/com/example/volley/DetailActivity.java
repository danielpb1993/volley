package com.example.volley;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView textNameDetail;
    private TextView textHeightDetail;
    private TextView textProminenceDetail;
    private TextView textZoneDetail;
    private ImageView imageViewDetail;
    private TextView textCountryDetail;

    String name, height, prominence, zone, url, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);

        textNameDetail = findViewById(R.id.textNameDetail2);
        textHeightDetail = findViewById(R.id.textHeightDetail2);
        textProminenceDetail = findViewById(R.id.textProminence2);
        textZoneDetail = findViewById(R.id.textZone2);
        imageViewDetail = findViewById(R.id.imageViewDetail2);
        textCountryDetail = findViewById(R.id.textCountryDetail2);

        getData();
        setData();
    }

    private void getData() {
        if (getIntent().hasExtra("name") &&
        getIntent().hasExtra("height")) {
            name = getIntent().getStringExtra("name");
            height = getIntent().getStringExtra("height");
            prominence = getIntent().getStringExtra("prominence");
            zone = getIntent().getStringExtra("zone");
            url = getIntent().getStringExtra("url");
            country = getIntent().getStringExtra("country");

        }
        else {
            Toast.makeText(this, "No data Found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        textNameDetail.setText(name);
        textHeightDetail.setText(height);
        textProminenceDetail.setText(prominence);
        textZoneDetail.setText(zone);
        textCountryDetail.setText(country);

        Picasso.get().load(url)
                .fit()
                .centerCrop()
                .into(imageViewDetail);
    }
}