package com.example.volley;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context mContext;
    private List<Peak> mPeak;

    public MyAdapter(Context mContext, List<Peak> mPeak) {
        this.mContext = mContext;
        this.mPeak = mPeak;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.peak_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.name.setText(mPeak.get(position).getName());
        holder.height.setText(mPeak.get(position).getHeight());
        holder.country.setText(mPeak.get(position).getCountry());
        Picasso.get().load(mPeak.get(position).getUrl())
                .fit()
                .centerCrop()
                .into(holder.url);
        holder.peakLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("name", mPeak.get(holder.getAdapterPosition()).getName());
                intent.putExtra("height", mPeak.get(holder.getAdapterPosition()).getHeight());
                intent.putExtra("prominence", mPeak.get(holder.getAdapterPosition()).getProminence());
                intent.putExtra("zone", mPeak.get(holder.getAdapterPosition()).getZone());
                intent.putExtra("country", mPeak.get(holder.getAdapterPosition()).getCountry());
                intent.putExtra("url", mPeak.get(holder.getAdapterPosition()).getUrl());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPeak.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView height;
        ImageView url;
        TextView country;

        ConstraintLayout peakLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textName);
            height = itemView.findViewById(R.id.textHeight);
            url = itemView.findViewById(R.id.imageUrl);
            country = itemView.findViewById(R.id.textCountry);
            peakLayout = itemView.findViewById(R.id.peak_row);
        }
    }
}
